/**
 * Axes.js
 * CREATES AND RETURNS A SET OF AXES
 * HAS BEEN MODIFIED TO PUSH THE AXES VALUES TO THE AxesList ARRAY, TO ALLOW THE AXES TO BE MODIFIED LATER
 */

AxesList = new Array();   // DECLARE THE ARRAY TO STORE THE VALUES FOR EACH OF THE AXES CREATED

function createAxes(length)
{
  var geometry = new THREE.Geometry();
  geometry.vertices.push(new THREE.Vector3(0, 0, 0));
  geometry.vertices.push(new THREE.Vector3(length, 0, 0));
  geometry.vertices.push(new THREE.Vector3(0, 0, 0));
  geometry.vertices.push(new THREE.Vector3(0, length, 0));
  geometry.vertices.push(new THREE.Vector3(0, 0, 0));
  geometry.vertices.push(new THREE.Vector3(0, 0, length));
  geometry.colors.push(new THREE.Color(0xff0000));
  geometry.colors.push(new THREE.Color(0xff0000));
  geometry.colors.push(new THREE.Color(0x00ff00));
  geometry.colors.push(new THREE.Color(0x00ff00));
  geometry.colors.push(new THREE.Color(0x0000ff));
  geometry.colors.push(new THREE.Color(0x0000ff));
  
  var material = new THREE.LineBasicMaterial();
  material.vertexColors = THREE.VertexColors;

  var axes = new THREE.LineSegments(geometry, material);
  axes.name = "axes";

  AxesList.push(axes) // STORE THE VALUES OF THE AXES CREATED INTO THE AxesList ARRAY.

  return axes;
}