/*###################################################################################################################################################################
#       FILENAME - Launcher.js                             WRITTEN USING SUMLIME TEXT 2 (LINESPACING 4)                                                             #
#                                                                                                                                                                   # 
#       BOB THE SWIMMER IN Javascript and Three.js                                                                                                                  #
#       WRITTEN BY CAMERON WATT S3589163                                                                                                                            #
#       COMPLETED 28 APRIL 2016                                                                                                                                     #
#                                                                                                                                                                   #
#  THIS PROGRAM SIMULATES A 3D MODEL OF A PERSON, AND ALLOWS JOINT ROTATIONS AND MATERIAL CHANGES TO THE MODEL IN REALTIME.                                         #
#                                                                                                                                                                   #
#  TO USE THIS PROGRAM, YOU NEED TO SELCT A JOINT (USING THE KEYBINDINGS BELOW) YOU WISH TO MANIPULATE AND MODIFY ITS ANGLE (WITHIN SET MINIMUM AND MAXIMUM ANGLES) #
#                                                                                                                                                                   #
#  THE KEYBINDINGS ARE AS FOLLOWS:                                                                                                                                  #
#                                                                                                                                                                   #
#  Left OR Right ARROW KEYS TO SELECT BETWEEN THE LEFT OR RIGHT JOINTS                                                                                              #
#   KEY   JOINT SELECTED   MINIMUM    MAXIMUM                                                                                                                       #
#    S      SHOULDER        -90         90                                                                                                                          #
#    E       ELBOW            0        180                                                                                                                          #
#    W       WRIST          -90         90                                                                                                                          #
#    H       HIP            -90         90                                                                                                                          #
#    K       KNEE           -90          0                                                                                                                          #
#    A       ANKLE            0         90                                                                                                                          #
#    N       NECK           -90         90                                                                                                                          #
#                                                                                                                                                                   #
#  ONCE THE REQUIRED JOINT IS SELECTED, SIMPLY PRESS THE Up OR Down KEYS TO ADJUST THE ANGLE OF THE SELECTED JOINT (WITHIN SET MINIMUM AND MAXIMUM ANGLES)          #
#                                                                                                                                                                   #
#  OTHER KEYBINDINGS ALLOW THE USER TO CONTROL THE LOOK OF THE MODEL, SUCH AS                                                                                       #
#                                                                                                                                                                   #
#    M   TOGGLES BETWEEN WIREFRAME AND FILLED MODE                                                                                                                  #
#    L   CHANGES BETWEEN STANDARD MESH (WITH LIGHTING HIDDEN) AND LAMBERT MESH (WITH LIGHTING VISIBLE)                                                              #
#    X   SHOW OR HIDE THE X, Y, Z AXES                                                                                                                              #
#                                                                                                                                                                   #
#  IF YOU WISH TO MODIFY THE SIZE OF THE MODEL, SIMPLY CHANGE THE GLOBAL sizeX, sizeY AND sizeZ VARIABLES. THIS WILL INCREASE THE SIZE OF THE SHAPES                #
#     THIS WILL ALSO MODIFY THE DISTANCES BETWEEN THEM SO THAT THE JOINTS STILL CONNECT CORRECTLY.                                                                  #
#     PLEASE NOTE THAT THE CAMERA POSITION IS ALSO A RELATION TO THE sizeX VARIABLE, ALLOWING THE CAMERA TO EFFECTIVELY SCALE WITH THE USERS CHOICE OF              #
#     X, Y, Z VALUES        PLEASE NOTE THAT sizeX IS THE MOST COMMONLY USED VARIABLE THROUGHT THE PROGRAM                                                          #
#                                                                                                                                                                   #
#  PLEASE NOTE THAT ALTHOUGH GLOBAL VARIABLES HAVE BEEN USED IN THIS PROGRAM, I HAVE TRIED TO INCLUDE THESE VARIABLES INTO THE FUNCION CALLS TO                     #
#          ALLOW EASIER RECOGNITION OF WHICH VARIABLES ARE USED WITHIN WHICH FUNCTIONS                                                                              #
#                                                                                                                                                                   #  
#  PLEASE NOTE THAT ALL ANGLES STORED INTO VARIABLES ARE IN DEGREES                                                                                                 #
#                                                                                                                                                                   #
#####################################################################################################################################################################

FUNCTION LIST - IN ORDER OF LISTING

window.onload = function()

function animate()  VOID
function render() VOID
function handleKeyDown(event)  VOID
function handleKeyUp(event)  VOID
function handleKeys(event) VOID
function changeAngles(adjustment)  VOID

    STORED WITHIN THE CreateSwimmer.js FILE
function createSwimmer(geometry, sizeX, sizeY, sizeZ, material, materialEyes) Return Torso
function createTorso(geometry, material)  Return Torso
function createJoint(name)  Return Joint
function createHead(geometry, sizeX, material)   Return Head
function createEyes(sizeX, materialEyes)  Return EYE
function createLimb(geometry, sizeX, sizeY, sizeZ, material)    Return Limb
function createOctahedron(geometry, sizeX, sizeY, sizeZ, material)  Return Octahedron
function createDecahedron(geometry, sizeX, sizeY, sizeZ, material)  Return Decahedron

###################################################################################################################################################################*/

// INITIALIZATION VARIABLES
var scene;
var camera;
var renderer;
var controls;

// KEY PRESS VARIABLES
window.onkeydown = handleKeyDown;
window.onkeyup = handleKeyUp;
var keysDown = [];

// STARTING MATERIALS FOR THE SWIMMER MODEL.  THE COLOUR OF THE MODEL CAN BE CHANGED HERE, AND WILL FILTER THROUGH TO EACH OF THE BODY PARTS (EXCEPT THE EYES)
//     THE CODE TO ACCOMPLISH THE HEX CHANGE WAS FOUND HERE: http://stackoverflow.com/questions/17463178/issue-in-coloring-mesh-in-three-js
var color = new THREE.Color( "#bf00ff" );
var hex = color.getHex();
var BasicMaterial = new THREE.MeshBasicMaterial( { color: hex } );
var LambertMaterial = new THREE.MeshLambertMaterial( { color: hex } )
var material = BasicMaterial;   // WHEN THE PROGRAM STARTS THIS SHOULD BE THE BASIC MATERIAL
material.wireframe = false;
var materialBasic = true;   // USED TO EASILY TRACK IF THE CURRENT MATERIAL IS BASIC (TRUE) OR LAMBERT (FALSE) 
                // I HAD TO USE THIS VARIABLE AS I COULDN'T WORK OUT HOW TO EVALUATE  if (X == THREE.MeshBasicMaterial)

// SET THE COLOR FOR THE EYES
var materialEyes = new THREE.MeshBasicMaterial( {color: 0x0000cc } );

// LIGHTING VARIABLES
var AmbientLight = new THREE.AmbientLight( 0x202020 ); 
var DirectionalLight  = new THREE.DirectionalLight( 0x909090, 1.0 );

// CREATE EMPTY SWIMMER VARIABLE TO HOLD THE FINAL STRUCTURE OF THE SWIMMER
var Swimmer =  undefined;

// GEOMETRY VARIABLES
var geometry;

// OCTAHEDRON AND DECAHEDRON SIZES      THESE CAN BE SET TO ALMOST ANY SIZE AND THE PROGRAM WILL CALCULATE THE NEW DIMENSIONS OF BOB
    // TESTED WITH (2, 1, 1)   (1.5, 1, 1)   (1, 0.6, 0.6)   (0.75, 0.45, 0.45)   
    // RECOMMENDED SIZES HERE ARE  0.5, 0.3, 0.3 AS THE CAMERA IS SET TO SHOW THIS WITHIN THE WINDOW FRAME
var sizeX = 0.5;
var sizeY = 0.3;
var sizeZ = 0.3;

var axesLength = 0.75;   // USED TO DETERMINE THE SIZE OF THE COLOURED AXIS BARS

var ShowAxes = true;  // BOOLEAN VALUE TO INDICATE IF THE AXES ARE SHOWN ON THE MODEL OR NOT

// JOINT MOVEMENT VARIABLES
var SideOfBody = 'Right';   // 'Right' or 'Left'
var TargetJoint = '';       // VARIABLE TO STORE THE NAME OF THE CURRENT JOINT
var TargetJointNo = 0;      // USED TO ALLOW EASY REFERENCE TO THE POSITIONS WITHIN THE ARRAY'S  [0-Shoulder, 1-Elbow, 2-Wrist, 3-Hip, 4-Knee, 5-Ankle]
                            // THIS WAS USED INSTEAD OF ENUMS, AS JAVASCRIPT DOESN'T HAVE INBUILD ENUMS
var TargetAngle = 0;        // ANGLE OF THE CURRENTLY TARGET JOINT
var TargetMin = 0;          // MINIMUM ANGLE OF THE CURRENT TARGET JOINT
var TargetMax = 0;          // MAXIMUM ANGLE OF THE CURRENT TARGET JOINT

// ANGLE MODEL IS TO BE ADJUST EACH TIME THE USER PRESSES THE Up OR Down ARROWS 
var AdjustmentInDegrees = 1;

// ANGLE OF THE SWIMMERS NECK - THIS WAS KEPT SEPERATE FROM THE ARRAYS, AS IT IS NEITHER Left OR Right
var NeckAngle = 0;

// ARRAY TO TRACK RIGHT ARM AND LEG ANGLES     READ LEFT TO RIGHT, TOP OF BODY TO THE BOTTOM, IE  [Shoulder, Elbow, Wrist, Hip, Knee, Ankle]
var RightSideAngles = [0, 0, 0, 0, 0, 0];

// ARRAY TO TRACK LEFT ARM AND LEG ANGLES  
var LeftSideAngles = [0, 0, 0, 0, 0, 0];

// ARRAYS TO TRACK THE MINIMUM AND MAXIMUM ANGLES FOR EACH JOINT   READ AS ABOVE - LEFT TO RIGHT, TOP OF BODY TO THE BOTTOM, IE  [Shoulder, Elbow, Wrist, Hip, Knee, Ankle]
// NOTE THE ANGLES FOR THE Knee HAD TO BE CHANGE FROM -90, 0    TO 0, 90 TO MAKE THE Knee MOVEMENT APPEAR MORE HUMAN, AND LESS ALIEN
var AngleMinimums = [-90, 0, -90, -90, 0, 0];
var AngleMaximums = [90, 180, 90, 90, 90, 90];

var BodyPartList = new Array(); // DECLARE THE ARRAY TO STORE THE VALUES FOR EACH OF THE PARTS CREATED - TO ALLOW MATERIAL CHANGES



window.onload = function()
{
scene = new THREE.Scene();

camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000); 
camera.position.z = (sizeX * 12);

DirectionalLight.position.set(0, 0.5, 1);
scene.add(AmbientLight, DirectionalLight);      // ADD THE LIGHTING TO THE SCENE
AmbientLight.visible = false;       // HIDE THESE, UNTIL THE MATERIAL IS CHANGED TO LAMBERT
DirectionalLight.visible = false;

// INITIALIZE THE TrackballControls
controls = new THREE.TrackballControls(camera);
controls.addEventListener('change', render);

// INITIALIZE THE RENDERER
renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor(0x404040, 1);
document.body.appendChild(renderer.domElement); 

Swimmer = createSwimmer(geometry, sizeX, sizeY, sizeZ, material, materialEyes);     // DRAW THE SWIMMER
scene.add(Swimmer);
animate();        // ANIMATING BOB
}


function animate()
{
  requestAnimationFrame(animate);
  render();

  controls.update();    // TO ALLOW TRACKBALL FUNCTIONALITY

  handleKeys();     // TO ALLOW KEYDOWN EVENTS
}

function render()
{
    renderer.render(scene, camera);
}


// CALLED WHEN A KEY IS PRESSED DOWN
function handleKeyDown(event)
{
    console.log('key: ' + event.keyCode + 'pressed');      // SHOWS UP THE EVENT CODE OF THE KEYPRESSED TO THE CONSOLE - KEPT FOR DEBUGGING PURPOSES
    keysDown[event.keyCode] = true;
}


// CALLED WHEN A KEY IS RELEASED
function handleKeyUp(event)
{
    // FUNCTIONS THAT NEED TO BE RUN ONCE EACH TIME THE KEY IS PRESSED TO STOP THE USER HOLDING THE KEY DOWN AND RE-RUNNING THE FUNCTION COUNTLESS TIMES
    switch (event.keyCode) 
    {
        case 77:    // M KEY - TOGGLE BETWEEN WIREFRAME AND FILLED MODE
        {
            material.wireframe = !material.wireframe;       // TOGGLE THE BOOLEAN VARIABLE material.wireframe TO BE EITHER true OR false
                    // COURTESY OF GEOFF'S TUTORIAL IN WEEK 8, THIS REPLACED THE ORIGINAL if(material.wireframe == true)
            break;
        }   // END OF case 77:   M KEY


        /*
        COURTESY OF CLARKES TUTORIAL IN WEEK 8. THIS CODE REPLACED A GREAT DEAL OF STATMENTS SUCH AS  Neck.visible = ShowAxes;     RightShoulder.visible = ShowAxes;  etc 
        I AM VERY GRATEFUL TO BE ABLE TO USE THIS PARTICULAR SECTION OF CODE IN THIS ASSIGNMENT, AS IT MAKES THIS FUNCTION (AND THE MATERIAL CHANGE) A HUNDRED TIMES EASIER.
        */
        case 88:       // X KEY - SHOW OR HIDE THE X, Y, Z AXES         
        {
            ShowAxes = !ShowAxes;       // TOGGLE THE BOOLEAN VARIABLE ShowAxes TO BE EITHER true OR false
            for (var i = 0; i < AxesList.length; i++)   // CYCLE THROUGH THE ARRAY AxesList AND SET EACH SET OF AXES TO BE VISIBLE, OR INVISIBLE (SEE ACKNOWLEDGEMENT ABOVE)
            {
                AxesList[i].visible = ShowAxes;
            }
            break;
        }   // END OF case 88:  X KEY


        case 76:      // L KEY - CHANGE BETWEEN STANDARD MESH (LIGHTING REMOVED) AND LAMBERT MESH (WITH LIGHTING)
        {
            if (materialBasic == false)    // IF CURRENT MATERIAL = Lambert, CHANGE TO Basic
            {
                material = BasicMaterial;      // SET THE MATERIAL OF THE SWIMMER TO Basic
                AmbientLight.visible = false;       // HIDE THESE, UNTIL THE MATERIAL IS CHANGED TO Lambert
                DirectionalLight.visible = false;
            }

            else if (materialBasic == true)   // IF CURRENT MATERIAL = Basic, CHANGE TO Lambert
            {
                material = LambertMaterial;      // SET THE MATERIAL OF THE SWIMMER TO Lambert
                AmbientLight.visible = true;       // SHOW THESE TO ILLUMINATE THE Lambert MATERIAL
                DirectionalLight.visible = true;
            }

            materialBasic = !materialBasic;     // SWAP VARIABLE BE ITS OPPOSITE BOOLEAN VALUE TO KEEP TRACK OF THE CURRENT MESH (TRUE = BASIC, FALSE = LAMBERT)
            material.wireframe = false;         // SET THE WIREFRAME TO FALSE TO SHOW THE CHANGES IN THE MATERIAL

            // COURTESY OF CLARKES TUTORIAL IN WEEK 8 - PLEASE SEE ACKNOWLEDGEMENT ABOVE case 88: 
            for (var i = 0; i < BodyPartList.length; i++)   // ITERATE THROUGH THE ARRAY BodyPartList AND SET EACH SET OF AXES TO BE VISIBLE, OR INVISIBLE
            {
                BodyPartList[i].material = material;
            }
            break;
        }   // END OF case 76:  L KEY


        //  JOINT SELECTION KEY PRESS FUNCTIONS
        case 37:         // LEFT Arrow Key - SELECT JOINTS ON THE  RIGHT SIDE OF THE BODY
        {
            SideOfBody = 'Left';
            break;
        }

        case 39:    // RIGHT Arrow Key - SELECT JOINTS ON THE RIGHT SIDE OF THE BODY
        {
            SideOfBody = 'Right';
            break;
        }

        case 83:    // S KEY - SELECT THE Shoulder Joint
        {
            TargetJoint = 'Shoulder';
            TargetJointNo = 0;      // JOINT REFERENCE WITHIN THE ARRAY'S RightSideAngles AND LeftSideAngles 
            break;
        }

        case 69:    // E KEY - SELECT THE Elbow Joint
        {
            TargetJoint = 'Elbow';
            TargetJointNo = 1;      // JOINT REFERENCE WITHIN THE ARRAY'S RightSideAngles AND LeftSideAngles 
            break;
        }

        case 87:    // W KEY - SELECT THE Wrist Joint
        {
            TargetJoint = 'Wrist';
            TargetJointNo = 2;      // JOINT REFERENCE WITHIN THE ARRAY'S RightSideAngles AND LeftSideAngles 
            break;
        }

        case 72:    // H KEY - SELECT THE Hip Joint
        {
            TargetJoint = 'Hip';
            TargetJointNo = 3;      // JOINT REFERENCE WITHIN THE ARRAY'S RightSideAngles AND LeftSideAngles             
            break;
        }

        case 75:    // E KEY - SELECT THE Knee Joint
        {
            TargetJoint = 'Knee';
            TargetJointNo = 4;      // JOINT REFERENCE WITHIN THE ARRAY'S RightSideAngles AND LeftSideAngles 
            break;
        }

        case 65:    // A KEY - SELECT THE Ankle Joint
        {
            TargetJoint = 'Ankle';
            TargetJointNo = 5;      // JOINT REFERENCE WITHIN THE ARRAY'S RightSideAngles AND LeftSideAngles 
            break;
        }

       case 78:    // N KEY - SELECT THE Neck Joint
        {
            TargetJoint = 'Neck';   // AS THE NECK IS NEITHER LEFT OR RIGHT, IT'S VARIABLES ARE NOT STORED IN THE ARRAY LIKE THE OTHER JOINTS
            break;
        } 
    }  // END OF SWITCH(Event.Keycode)


    // ONCE THE RESPECTIVE JOINT HAS BEEN SELECTED, SET ITS RESPECTIVE ANGLE TO TargetAngle TO ALLOW EASY COMPARISON WITHIN THE UP AND DOWN KEY EVENTS
    if (TargetJoint == 'Neck')
    {
        TargetAngle = NeckAngle;     // AS THE NECK IS NEITHER LEFT OR RIGHT, IT'S VARIABLES ARE NOT STORED IN THE ARRAY LIKE THE OTHER JOINTS
        TargetMin = -90;
        TargetMax = 90;  
    }
    else
    {
        if (SideOfBody == 'Right')
        {
            TargetAngle = RightSideAngles[TargetJointNo];
        }
        else if (SideOfBody == 'Left')
        {
            TargetAngle = LeftSideAngles[TargetJointNo];
        }
        TargetMin = AngleMinimums[TargetJointNo];
        TargetMax = AngleMaximums[TargetJointNo];
    }

    console.log('key: ' + event.keyCode + 'released');      // SHOWS UP THE EVENT CODE OF THE KEYPRESSED TO THE CONSOLE - KEPT FOR DEBUGGING PURPOSES
    keysDown[event.keyCode] = false;
} // END OF handleKeyUp(event)



// REPEATITIVE KEY PRESS EVENTS - MOSTLY PRESS AND HOLD TYPE EVENTS
function handleKeys(event)
{
    if(keysDown[38])   // UP Arrow Key PRESSED     INCREASE THE ANGLE OF THE SELECTED JOINT
    {
        if (TargetAngle < TargetMax)
        {
            changeAngles(AdjustmentInDegrees);            // POSITIVE ADJUSTMENT - ONE DEGREE CONVERTED TO RADIANS
            TargetAngle += AdjustmentInDegrees;           // ADJUST TARGET ANGLE TO ALLOW TRACKING OF MIN AND MAX
            render();
        }
    }   // END OF UP ARROW PRESSED

    else if(keysDown[40])    // DOWN Arrow Key PRESSED     DECREASE THE ANGLE OF THE SELECTED JOINT
    {
        if (TargetAngle > TargetMin)
        {
            changeAngles(-AdjustmentInDegrees);     // NEGATIVE ADJUSTMENT - ONE DEGREE CONVERTED TO RADIANS
            TargetAngle -= AdjustmentInDegrees;           // ADJUST TARGET ANGLE TO ALLOW TRACKING OF MIN AND MAX
            render();
        }
    }   // END OF DOWN ARROW PRESSED
}

/*  PLEASE NOTE - THIS FUNCTION ACCEPTS ANGLES IN DEGREES AND CONVERTS THEM TO RADIANS TO ADJUST THE MODEL, THEN RETURNS THE ANGLE BACK IN DEGREES TO BE STORED IN THE
        RELEVANT JOINTS ANGLE VARIABLE.
    THIS FUNCTION IS CALLED WHEN THE DOWN ARROW IS PRESSED, IT ADJUST'S THE ANGLE OF THE TargetJoint, THEN UPDATES THE AdjustmentInDegrees TO THE GLOBAL VARIABLE FOR
        THE RESPECTIVE TargetJoint's ANGLE  */
function changeAngles(AdjustmentInDegrees)
{ 
    var AdjustmentInRadians = (AdjustmentInDegrees * Math.PI / 180) // CONVERT THE ADJUSTMENT FROM DEGREES TO RADIANS AND STORE THE RESULT IN THIS VARIABLE.  
            // ALL ADJUSTMENT VALUES THROUGHOUT THE PROGRAM ARE IN DEGREES.  THIS CONVERTED VALUE IS THE ONLY ANGLE IN RADIANS WITHIN THE PROGRAM, AND IS ONLY USED WITHIN THIS FUNCTION
    if (TargetJoint == 'Neck')   // CHANGE THE ANGLE OF THE NECK, WITHOUT EXECUTING THE REST OF THE STATEMENTS AS IT IS NEITHER RIGHT OR LEFT
    {
        Swimmer.Neck.rotation.y += AdjustmentInRadians;        
        NeckAngle += AdjustmentInDegrees;  // STORE THE NEWLY ADJUSTED ANGLE INTO THE GLOBAL VARIABLE IN DEGREES
    }
    else if (SideOfBody == 'Right')   // 'Right' or 'Left'
    {
        switch (TargetJoint) 
        {
        // RIGHT ARM ROTATIONS
        case 'Shoulder':
            Swimmer.RightArm.UpperJoint.rotation.z += (AdjustmentInRadians);
            break;
        case 'Elbow':
            Swimmer.RightArm.CentralJoint.rotation.z += (AdjustmentInRadians);
            break;
        case 'Wrist':
            Swimmer.RightArm.LowerJoint.rotation.z += (AdjustmentInRadians);
            break;

        // RIGHT LEG ROTATIONS
        case 'Hip':
            Swimmer.RightLeg.UpperJoint.rotation.y += (AdjustmentInRadians);
            break;
        case 'Knee':
            Swimmer.RightLeg.CentralJoint.rotation.y += (AdjustmentInRadians);
            break;
        case 'Ankle':
            Swimmer.RightLeg.LowerJoint.rotation.y += (AdjustmentInRadians);
            break;
        }
        RightSideAngles[TargetJointNo] += AdjustmentInDegrees;   // STORE THE NEWLY ADJUSTED ANGLE INTO THE GLOBAL VARIABLE IN DEGREES
    }   // END OF RIGHT SIDE OF BODY

    else if (SideOfBody == 'Left')
    {
        switch (TargetJoint) 
        {
        // LEFT ARM ROTATIONS
        case 'Shoulder':
            Swimmer.LeftArm.UpperJoint.rotation.z += (AdjustmentInRadians);   
            break;
        case 'Elbow':
            Swimmer.LeftArm.CentralJoint.rotation.z += (AdjustmentInRadians);
            break;
        case 'Wrist':
            Swimmer.LeftArm.LowerJoint.rotation.z += (AdjustmentInRadians);
            break;

        // LEFT LEG ROTATIONS
        case 'Hip':
            Swimmer.LeftLeg.UpperJoint.rotation.y += (AdjustmentInRadians);
            break;
        case 'Knee':
            Swimmer.LeftLeg.CentralJoint.rotation.y += (AdjustmentInRadians);
            break;
        case 'Ankle':
            Swimmer.LeftLeg.LowerJoint.rotation.y += (AdjustmentInRadians);
            break;
        }  
        LeftSideAngles[TargetJointNo] += AdjustmentInDegrees;   // STORE THE NEWLY ADJUSTED ANGLE INTO THE GLOBAL VARIABLE IN DEGREES
    }   // END OF LEFT SIDE OF BODY
}   // END OF changeAngles(AdjustmentInDegrees) FUNCTION

