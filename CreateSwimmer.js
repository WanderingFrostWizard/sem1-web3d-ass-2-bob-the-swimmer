/*###################################################################################################################################################################
#       FILENAME - CreateSwimmer.js                             WRITTEN USING SUMLIME TEXT 2 (LINESPACING 4)                                                        #
#                                                                                                                                                                   # 
#       BOB THE SWIMMER IN Javascript and Three.js                                                                                                                  #
#       WRITTEN BY CAMERON WATT S3589163                                                                                                                            #
#       COMPLETED 28 APRIL 2016                                                                                                                                     #
#                                                                                                                                                                   #
#       THIS FILE CREATES THE SWIMMER USED WITHIN THE Launcher.js file                                                                                              #
#                                                                                                                                                                   #
###################################################################################################################################################################*/

// ALL OTHER GLOBAL VARIABLES HAVE BEEN LEFT IN THE Launcher.js file

    // VARIABLES USED TO DETERMINE THE COORDINATES OF THE TORSO, WHICH IS ALSO USED TO DETERMINE THE PLACEMENT OF THE ARMS, LEGS AND NECK.
    var TorsoWidthA = (2 * sizeX * Math.sin(1.25664));      //  0.95      72 Degrees     
    var TorsoHeightA = (2 * sizeX * Math.cos(1.25664));     //  0.31
    
    var TorsoWidthB = (2 * sizeX * Math.sin(2.51327));      //  0.58      144 Degrees     
    var TorsoHeightB = (2 * sizeX * Math.cos(2.51327));     // -0.81

function createSwimmer(geometry, sizeX, sizeY, sizeZ, material, materialEyes)
{
    // ADD TORSO
    var Torso = new createTorso(geometry, sizeX, material, TorsoWidthA, TorsoHeightA, TorsoWidthB, TorsoHeightB);

    // MOVE TO NECK POSITION AND PLACE NECK JOINT
    var Neck = new createJoint("Neck");
    Torso.Neck = Neck;
    Neck.position.y += -TorsoHeightB;
    Neck.add(createAxes(axesLength));

    // MOVE TO HEAD POSITON AND PLACE HEAD
    var Head = new createHead(geometry, sizeX, material);
    Head.position.y += sizeX;

    var RightEye = new createEyes(sizeX, materialEyes);
    RightEye.position.x += (sizeX/4);
    RightEye.position.y += (sizeX/4);
    RightEye.position.z += (sizeX/4);

    var LeftEye = new createEyes(sizeX, materialEyes);
    LeftEye.position.x -= (sizeX/4);
    LeftEye.position.y += (sizeX/4);
    LeftEye.position.z += (sizeX/4);

    // ATTACH JOINTS TOGETHER AND TO THE TORSO
    Head.add(RightEye);
    Head.add(LeftEye);
    Neck.add(Head);
    Torso.add(Neck);      



    // ##### RIGHT ARM #####            
    // DRAW THE ARM
    RightArm = new createLimb(geometry, sizeX, sizeY, sizeZ, material)
    Torso.RightArm = RightArm;

    // SHIFT RIGHT ARM INTO POSITION
    RightArm.position.x += TorsoWidthA;
    RightArm.position.y += TorsoHeightA;

    // ADD THE ENTIRE RIGHT ARM TO THE TORSO                      
    Torso.add(RightArm);



    // ##### LEFT ARM #####
    // DRAW THE ARM
    LeftArm = new createLimb(geometry, sizeX, sizeY, sizeZ, material)
    Torso.LeftArm = LeftArm;

    // ROTATE THEN SHIFT LEFT ARM INTO POSITION
    LeftArm.rotation.z = 3.14;  // ROTATE 180 DEGREES TO LEFT SIDE OF THE BODY
    LeftArm.rotation.x = 3.14;  // ROTATE 180 DEGREES THE LEFT ARM SO THAT THE AXIS ARE IN THE CORRECT ORIENTATION
    LeftArm.position.x += -TorsoWidthA;
    LeftArm.position.y += TorsoHeightA;

    // ADD THE ENTIRE LEFT ARM TO THE TORSO
    Torso.add(LeftArm);



    // ##### RIGHT LEG #####
    // DRAW THE LEG
    RightLeg = new createLimb(geometry, sizeX, sizeY, sizeZ, material)
    Torso.RightLeg = RightLeg;

    // ROTATE THEN SHIFT RIGHT LEG INTO POSITION
    RightLeg.rotation.z = (-3.14/2);    // ROTATE 90 DEGREES
    RightLeg.position.x += TorsoWidthB;
    RightLeg.position.y += TorsoHeightB;

    // ADD THE ENTIRE RIGHT LEG TO THE TORSO
    Torso.add(RightLeg);



    // ##### LEFT LEG #####
    // DRAW THE LEG
    LeftLeg = new createLimb(geometry, sizeX, sizeY, sizeZ, material)
    Torso.LeftLeg = LeftLeg;

    // ROTATE THEN SHIFT LEFT LEG INTO POSITION
    LeftLeg.rotation.z = (-3.14/2);    // ROTATE 90 DEGREES
    LeftLeg.position.x += -TorsoWidthB;
    LeftLeg.position.y += TorsoHeightB;

    // ADD THE ENTIRE LEFT LEG TO THE TORSO
    Torso.add(LeftLeg);

    return Torso;            
}   // END OF CreateSwimmer(material, materialEyes);



// CREATES AN OBJECT TO REPRESENT THE SWIMMERS Torso
function createTorso(geometry, sizeX, material, TorsoWidthA, TorsoHeightA, TorsoWidthB, TorsoHeightB)                                        
{
/*                                  
                                           X 0, -TorsoHeightB  (Point 0)
                                         #   #
                                       #       #
(Point 4) -TorsoWidthA, TorsoHeightA X           X TorsoWidthA, TorsoHeightA   (Point 1)
                                      #    0    #
                                       #       #
   (Point 3) -TorsoWidthB, TorsoHeightB X # # X  TorsoWidthB, TorsoHeightB  (Point 2)
*/
    geometry = new THREE.Geometry();

    // SHAPE OUTLINE VERTICES
    geometry.vertices.push(new THREE.Vector3(0, -TorsoHeightB, 0));             // (Point 0)
    geometry.vertices.push(new THREE.Vector3(TorsoWidthA, TorsoHeightA, 0));    // (Point 1)
    geometry.vertices.push(new THREE.Vector3(TorsoWidthB, TorsoHeightB, 0));    // (Point 2)
    geometry.vertices.push(new THREE.Vector3(-TorsoWidthB, TorsoHeightB, 0));   // (Point 3)
    geometry.vertices.push(new THREE.Vector3(-TorsoWidthA, TorsoHeightA, 0));   // (Point 4)

    // DEPTH VERTICES
    geometry.vertices.push(new THREE.Vector3(0, 0, sizeX));
    geometry.vertices.push(new THREE.Vector3(0, 0, -sizeX));

    // TOP LAYER OF FACES
    geometry.faces.push(new THREE.Face3(0, 5, 1));
    geometry.faces.push(new THREE.Face3(1, 5, 2));
    geometry.faces.push(new THREE.Face3(2, 5, 3));
    geometry.faces.push(new THREE.Face3(3, 5, 4));
    geometry.faces.push(new THREE.Face3(4, 5, 0));
    
    // BOTTOM LAYER OF FACES
    geometry.faces.push(new THREE.Face3(1, 6, 0));
    geometry.faces.push(new THREE.Face3(2, 6, 1));
    geometry.faces.push(new THREE.Face3(3, 6, 2));
    geometry.faces.push(new THREE.Face3(4, 6, 3));
    geometry.faces.push(new THREE.Face3(0, 6, 4));

    geometry.computeFaceNormals();

    Torso = new THREE.Mesh(geometry, material); 

    Torso.name = "Torso";
    Torso.add(createAxes(axesLength));

    BodyPartList.push(Torso);       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY (FOR MATERIAL CHANGES)

    return Torso;
}


// CREATE THE JOINT OBJECT AND ADD THE AXIS TO THE POINT
function createJoint(name)
{
    var Joint = new THREE.Object3D();
    Joint.add(createAxes(axesLength));

    return Joint;
}


// CREATES AN OBJECT TO REPRESENT THE SWIMMERS Head   
// PLEASE NOTE - I AM USING THE sizeX VARIABLE IN THE PLACE OF A Y AND Z VALUE, THIS IS INTENTIONAL TO MAINTAIN A CONSISTENT SHAPE FOR THE HEAD IN COMPARISON TO THE BODY
function createHead(geometry, sizeX, material)            
{
    geometry = new THREE.Geometry();

    // SHAPE OUTLINE VERTICES
    geometry.vertices.push(new THREE.Vector3(0, sizeX, 0));
    geometry.vertices.push(new THREE.Vector3(sizeX, 0, 0));
    geometry.vertices.push(new THREE.Vector3(0, -sizeX, 0));
    geometry.vertices.push(new THREE.Vector3(-sizeX, 0, 0));

    // DEPTH VERTICES
    geometry.vertices.push(new THREE.Vector3(0, 0, sizeX));
    geometry.vertices.push(new THREE.Vector3(0, 0, -sizeX));


    // FRONT TOP HALF FRONT FACES
    geometry.faces.push(new THREE.Face3(0, 3, 4));
    geometry.faces.push(new THREE.Face3(0, 4, 1));

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push(new THREE.Face3(2, 4, 3));
    geometry.faces.push(new THREE.Face3(2, 1, 4));
   
    // REAR TOP HALF FRONT FACES
    geometry.faces.push(new THREE.Face3(0, 5, 3));
    geometry.faces.push(new THREE.Face3(0, 1, 5));

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push(new THREE.Face3(2, 3, 5));
    geometry.faces.push(new THREE.Face3(2, 5, 1));

    geometry.computeFaceNormals();

    Head = new THREE.Mesh(geometry, material); 

    Head.name = "Head";
    Head.add(createAxes(axesLength));

    BodyPartList.push(Head);       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY (FOR MATERIAL CHANGES)

    return Head;
}


// CREATES AND RETURNS A Spherical SHAPE TO REPRESENT THE EYES OF THE SWIMMER
function createEyes(sizeX, materialEyes)                     
{
    var geometry = new THREE.SphereGeometry(sizeX/4);

    geometry.computeFaceNormals();

    eye = new THREE.Mesh(geometry, materialEyes); 

    eye.name = "eye";
    return eye;
}


// CREATES ALL THE OBJECTS NEEDED TO CREATE AN ARM OR A LEG 
function createLimb(geometry, sizeX, sizeY, sizeZ, material)
{
    var Limb = new THREE.Object3D();

    // CREATE UpperJoint - USED FOR Shoulder OR Hip
    var UpperJoint = new createJoint(UpperJoint);
    UpperJoint.name = "UpperJoint";

    // CREATE UpperLimb AND MOVE TO POSITION
    var UpperLimb = new createOctahedron(geometry, sizeX, sizeY, sizeZ, material);
    UpperLimb.name = "UpperLimb"; 
    UpperLimb.position.x += sizeX;

    // CREATE CentralJoint AND MOVE TO POSITION - USED FOR Elbow OR Knee
    var CentralJoint = new createJoint(CentralJoint);
    CentralJoint.name = "CentralJoint";
    CentralJoint.position.x += sizeX;

    // CREATE LowerLimb AND MOVE TO POSITION
    var LowerLimb = new createOctahedron(geometry, sizeX, sizeY, sizeZ, material);
    LowerLimb.name = "LowerLimb";
    LowerLimb.position.x += sizeX; 

    // CREATE LowerJoint AND MOVE TO POSITION - USED FOR Wrist OR Ankle
    var LowerJoint = new createJoint(LowerJoint);
    LowerJoint.name = "LowerJoint";
    LowerJoint.position.x += sizeX;

    // CREATE HAND AND MOVE TO POSITION
    var Hand = new createDecahedron(geometry, sizeX, sizeY, sizeZ, material);  
    Hand.name = "Hand";

    // CONNECT THE HEIRACHY OF THE LIMB BEFORE RETURNING IT TO THE CALLING FUNCTION
    Limb.add(UpperJoint);
    UpperJoint.add(UpperLimb);
    UpperLimb.add(CentralJoint);
    CentralJoint.add(LowerLimb);
    LowerLimb.add(LowerJoint);
    LowerJoint.add(Hand);

    Limb.UpperJoint = UpperJoint;
    Limb.UpperLimb = UpperLimb;
    Limb.CentralJoint = CentralJoint;
    Limb.LowerLimb = LowerLimb;
    Limb.LowerJoint = LowerJoint;
    Limb.Hand = Hand;

return Limb;
}


// CREATES AND RETURNS AN Octahedron OBJECT TO REPRESENT THE LIMBS OF THE SWIMMER
function createOctahedron(geometry, sizeX, sizeY, sizeZ, material)                 
{
    geometry = new THREE.Geometry();

    // SHAPE OUTLINE VERTICES
    geometry.vertices.push(new THREE.Vector3(0, sizeY, 0));
    geometry.vertices.push(new THREE.Vector3(sizeX, 0, 0));
    geometry.vertices.push(new THREE.Vector3(0, -sizeY, 0));
    geometry.vertices.push(new THREE.Vector3(-sizeX, 0, 0));

    // DEPTH VERTICES
    geometry.vertices.push(new THREE.Vector3(0, 0, sizeZ));
    geometry.vertices.push(new THREE.Vector3(0, 0, -sizeZ));


    // FRONT TOP HALF FRONT FACES
    geometry.faces.push(new THREE.Face3(0, 3, 4));
    geometry.faces.push(new THREE.Face3(0, 4, 1));

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push(new THREE.Face3(2, 4, 3));
    geometry.faces.push(new THREE.Face3(2, 1, 4));
   
    // REAR TOP HALF FRONT FACES
    geometry.faces.push(new THREE.Face3(0, 5, 3));
    geometry.faces.push(new THREE.Face3(0, 1, 5));

    // FRONT BOTTOM HALF FRONT FACES  
    geometry.faces.push(new THREE.Face3(2, 3, 5));
    geometry.faces.push(new THREE.Face3(2, 5, 1));

    geometry.computeFaceNormals();

    Octahedron = new THREE.Mesh(geometry, material); 

    Octahedron.name = "Octahedron";
    Octahedron.add(createAxes(axesLength));

    BodyPartList.push(Octahedron);       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY

    return Octahedron;
}


// CREATES AND RETURNS A Decahedron OBJECT TO REPRESENT THE HANDS AND FEET OF THE SWIMMER
function createDecahedron(geometry, sizeX, sizeY, sizeZ, material)                     
{
    geometry = new THREE.Geometry();

    // APEX OF SHAPE
    geometry.vertices.push(new THREE.Vector3(0, 0, 0));

    // BASE OF SHAPE
    geometry.vertices.push(new THREE.Vector3(sizeX, sizeY, sizeZ));
    geometry.vertices.push(new THREE.Vector3(sizeX, -sizeY, sizeZ));
    geometry.vertices.push(new THREE.Vector3(sizeX, -sizeY, -sizeZ));
    geometry.vertices.push(new THREE.Vector3(sizeX, sizeY, -sizeZ));


    // UPWARD FACING SIDE OF SHAPE
    geometry.faces.push(new THREE.Face3(1, 4, 0));

    // FORWARD FACING SIDE OF SHAPE
    geometry.faces.push(new THREE.Face3(2, 1, 0));

    // DOWNWARD FACING SIDE OF SHAPE
    geometry.faces.push(new THREE.Face3(3, 2, 0));
   
    // REAR FACING SIDE OF SHAPE
    geometry.faces.push(new THREE.Face3(4, 3, 0));

    // SQUARE BASE OF SHAPE
    geometry.faces.push(new THREE.Face3(3, 4, 1));
    geometry.faces.push(new THREE.Face3(1, 2, 3));

    geometry.computeFaceNormals();

    Decahedron = new THREE.Mesh(geometry, material); 

    Decahedron.name = "Decahedron";

    BodyPartList.push(Decahedron);       // ADD THIS NEWLY CREATED BODY PART TO THE BodyPartList ARRAY

    return Decahedron;
}